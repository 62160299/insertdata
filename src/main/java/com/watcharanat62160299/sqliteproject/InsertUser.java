/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watcharanat62160299.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class InsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + "user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
         String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) " +
                        "VALUES (1, 'Paul',123456);"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) " +
                  "VALUES (2, 'Peter',238842);"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) " +
                  "VALUES (3, 'Puki',331248);"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) " +
                  "VALUES (4, 'Penny',502030);"; 
         stmt.executeUpdate(sql);
         conn.commit();
         conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
}
