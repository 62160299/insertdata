/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watcharanat62160299.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class DeleteUser {
     public static void main(String[] args) 
     {
        Connection conn = null;
        Statement stmt = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            
            stmt.executeUpdate("Delete from USER where ID = 1 ");
            conn.commit();
            
            ResultSet rs = stmt.executeQuery( "SELECT * FROM user;" );
         while ( rs.next() ) {
         int id = rs.getInt("id");
         String  username = rs.getString("USERNAME");
         int password  = rs.getInt("PASSWORD");
  
         System.out.println( "ID = " + id );
         System.out.println( "NAME = " + username );
         System.out.println( "PASSWORD = " + password );
         System.out.println();
      }
         rs.close();
         stmt.close();
         conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
